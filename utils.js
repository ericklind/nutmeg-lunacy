#!/usr/bin/env node
'use strict';
import crypto from 'crypto';

const utils = {
	generateDepositAddress() {
		const hash = crypto.createHash('sha256');
		return hash.update(`${Date.now()}`).digest('hex').substring(0, 8);
	},
};

export default utils;
