import chalk from 'chalk';
import pkg from 'inquirer';
import API from './apiClient.js';

const { prompt: _prompt } = pkg;
const { green } = chalk;

function client() {
	/* Inquirer documentation: https://github.com/SBoudrias/Inquirer.js#documentation */
	_prompt([
		{
			name: 'sendFrom',
			message: 'Enter the address to send from:',
		},
		{
			name: 'sendTo',
			message: 'Enter the address to send to:',
		},
		{
			name: 'amount',
			message: 'Enter the amount to send:',
		},
	]).then((answers) => {
		console.log(answers);
		let api = new API();
		let { sendFrom, sendTo, amount } = answers;
		if (sendFrom && sendTo && amount) {
			api
				.getAddresses(sendFrom)
				.then((data) => {
					if (data && data.balance) {
						if (data.balance >= amount)
							api
								.sendTransaction(sendFrom, sendTo, amount)
								.then((d) => {
									console.log('Coins have been sent!');
								})
								.catch((error) => {
									throw error;
								});
					}
				})
				.catch((error) => {
					throw error;
				});
		} else {
			console.log('Unable to send due to invalid address or amount!');
		}
	});
}
console.log('Welcome to the Jobcoin client!');
client();

export default client;
