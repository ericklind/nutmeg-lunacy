const TestData = (depositAddress) => {
	return {
		//Mock data for the users wallet
		addresses: {
			balance: 37.5,
			transactions: [
				{
					timestamp: '2021-06-02T13:47:11.184Z',
					fromAddress: 'Gilligan',
					toAddress: depositAddress,
					amount: '37.5',
				},
			],
		},
		//Master list of mock transactions
		transactions: [
			{
				timestamp: '2021-06-02T13:47:11.184Z',
				fromAddress: 'Gilligan',
				toAddress: depositAddress,
				amount: '37.5',
			},
			{
				timestamp: '2021-06-02T13:47:11.180Z',
				toAddress: 'Gilligan',
				amount: '50',
			},
			{
				timestamp: '2021-06-02T13:47:11.184Z',
				fromAddress: 'Alice',
				toAddress: 'Bob',
				amount: '12.5',
			},
		],
		//Master list of new addresses
		sendAddresses: ['Skpper', 'Ginger', 'Maryanne'],
	};
};
export default TestData;
