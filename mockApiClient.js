class MockAPI {
	constructor(testData) {
		this.testData = testData;
	}
	//This will get the data for the deposit address
	getAddresses = (address) => {
		return this.testData.addresses;
	};
	getTransactions = () => {
		return this.testData.transactions;
	};
	sendTransaction = (fromAddress, toAddress, amount) => {
		console.log('SENT!', fromAddress, toAddress, amount);
		return { status: 'OK' };
	};
}
export default MockAPI;
