const HOUSE_ADDRESS = 'house';
const FEE_ADDRESS = 'fees';
const FEE_AMOUNT = 0.1;
const MIN_INTERVAL = 1000;
const MAX_INTERVAL = 10000;

//Math.round seems to be unreliable, so using this.
function round(value, decimals) {
	return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
}

class MixerJob {
	constructor(API, addresses, depositAddress) {
		this.api = API;
		this.addresses = addresses;
		this.depositAddress = depositAddress;
	}
	/**
	 * Send the fee amounts from the house to the fee account
	 * @param {number} fee
	 * @returns {status: "OK"}
	 */
	collectFee = async (fee) => {
		try {
			let data = await this.api.getAddresses(HOUSE_ADDRESS);
			// console.log(data);
			if (data && data.balance >= fee) {
				return this.api.sendTransaction(HOUSE_ADDRESS, FEE_ADDRESS, fee);
			} else {
				//Sad Panda!
				throw new Error('Unable to collect fee due to insufficient funds!');
			}
		} catch (error) {
			console.log(error);
			throw error;
		}
	};
	/**
	 * Move the initial deposit to the new deposit address.
	 * Not using this at the moment, but could integrate it into the client
	 * @param {string} address
	 * @param {number} amount
	 */
	deposit = async (address, amount) => {
		try {
			//Check the balance first!
			let data = this.api.getAddresses(address);
			// console.log(data);
			if (data && parseFloat(data.balance) >= amount) {
				//We're flush!
				return this.api.sendTransaction(address, this.depositAddress, amount);
			} else {
				//Sad Panda!
				throw new Error(
					'Unable to complete transactions due to insufficient funds!'
				);
			}
		} catch (error) {
			throw error;
		}
	};
	/**
	 * Transfer the deposit from deposit account to house account
	 * @param {number} amount
	 * @returns void
	 */
	transfer = async (amount) => {
		try {
			let data = await this.api.getAddresses(this.depositAddress);
			// console.log(data);
			if (data && parseFloat(data.balance) >= amount) {
				return this.api.sendTransaction(
					this.depositAddress,
					HOUSE_ADDRESS,
					amount
				);
			} else {
				//Sad Panda!
				throw new Error(
					'Unable to complete transfer due to insufficient funds!'
				);
			}
		} catch (error) {
			console.log(error);
			throw error;
		}
	};
	/**
	 * Send the amounts to the unused addresses
	 * @param {array} transactions
	 * @param {array} intervals
	 */
	disburse = async (transactions, intervals) => {
		try {
			let i = 0;
			let count = transactions.length;
			let self = this; //Get the correct scope
			let timerId = setTimeout(async function tick() {
				if (i >= count) {
					clearTimeout(timerId);
					return { status: 'COMPLETED' };
				}
				let { toAddress, amount, fee } = transactions[i];

				// console.log(`disbursing! ${toAddress}`);

				await self.collectFee(fee);

				let result = await self.api.sendTransaction(
					HOUSE_ADDRESS,
					toAddress,
					amount
				);

				// console.log(result);
				i++;
				timerId = setTimeout(tick, intervals[i]);
			}, intervals[i]);
		} catch (error) {
			console.log(error);
			throw error;
		}
	};
	/**
	 * Mixes up the balance into the required number of amounts.
	 * @param {number} balance
	 * @param {number} count
	 * @returns Array of mixed about, which sum up to the balance amount.
	 */
	mixBalance = (balance, count) => {
		try {
			//Will need to revisit this. Math.floor was not workng exepcted.
			let amount = parseInt(balance, 10);
			//First we get the whole amount
			//Check for decimals. If so mix it up too.
			let d = balance.toString().split('.'); // Get the decimals if any
			let decAmount = d.length > 0 ? d[1] : 0;
			let precision = decAmount.toString().length;
			let mixedDec = this.mixAmount(decAmount, count).map((d) =>
				parseFloat('.' + d)
			);
			let balances = this.mixAmount(amount, count);

			//Zip the amounts together
			return balances.map((b, i) =>
				parseFloat(round(b + mixedDec[i], precision))
			);
		} catch (error) {
			console.log(error);
			throw error;
		}
	};
	/**
	 * Randomizes and splits up an amount into a number of amounts
	 * This is used for the whole number and the decimal amounts.
	 * @param {number} amount
	 * @param {number} count
	 * @returns Array of mixed and split amounts
	 */
	mixAmount = (amount, count) => {
		try {
			let max = parseInt(amount / count + 1, 10); //Just adding a little randomness to the mix. This should be replaced, but it will do for now.
			let balances = new Array(count);
			let sum = 0;
			do {
				for (let i = 0; i < count; i++) {
					balances[i] = Math.random();
				}
				sum = balances.reduce((acc, val) => acc + val, 0);
				const scale = (amount - count) / sum;
				balances = balances.map((val, i) =>
					Math.min(max, Math.round(val * scale) + 1)
				);
				sum = balances.reduce((acc, val) => acc + val, 0);
			} while (sum - amount);

			return balances;
		} catch (error) {
			console.log(error);
			throw error;
		}
	};
	/**
	 * Get random interval for sending transactions
	 * @param {number} min
	 * @param {number} max
	 * @param {number} count
	 * @returns Array of random intervals between the min and max
	 */
	randomIntervals = (min, max, count) => {
		try {
			let intervals = new Array(count);
			for (let i = 0; i < intervals.length; i++) {
				intervals[i] = parseInt(Math.random() * (max - min) + min, 10);
			}
			return intervals;
		} catch (error) {
			console.log(error);
			throw error;
		}
	};
	/**
	 * Main entry point for mixing balances.
	 * @param {number} balance
	 * @returns Returns { status: 'COMPLETED'}
	 */
	mix = async (balance) => {
		try {
			let count = this.addresses.length;
			let d = balance.toString().split('.'); // Get the decimals if any
			let decAmount = d.length > 0 ? d[1] : 0;
			let precision = decAmount.toString().length;
			//Mix up the amounts
			let mixedAmounts = this.mixBalance(balance, count);
			//Create transactions
			let transactions = this.addresses.map((address, i) => {
				return {
					fee: FEE_AMOUNT,
					toAddress: address,
					amount: parseFloat(round(mixedAmounts[i] - FEE_AMOUNT, precision)),
				};
			});
			// console.log('mix.transactions', transactions);
			//Transfer from deposit account to house account
			await this.transfer(balance);
			//Get random intervals
			let intervals = this.randomIntervals(MIN_INTERVAL, MAX_INTERVAL, count);
			//Disburse the amounts to the accounts
			let data = await this.disburse(transactions, intervals);

			return data;
		} catch (error) {
			console.log(error);
		}
	};
	/**
	 * Method to poll the API to check if the deposit account has a balance > 0
	 */
	poll = async () => {
		try {
			console.log(`Polling address ${this.depositAddress}`);
			let results = await this.api.getAddresses(this.depositAddress);
			let { balance } = results;
			if (balance > 0) {
				return this.mix(parseFloat(balance));
			} else {
				//Return zero balance.
				return { status: 'ZERO' };
			}
		} catch (error) {
			console.log(error);
		}
	};
}
export default MixerJob;
