import pkg from 'axios';
const { get, post } = pkg;

/* Urls */
const API_BASE_URL = 'https://jobcoin.gemini.com/nutmeg-lunacy/api';
const API_ADDRESS_URL = `${API_BASE_URL}/addresses`;
const API_TRANSACTIONS_URL = `${API_BASE_URL}/transactions`;
class API {
	getAddresses = (address) => {
		// console.log('ADDRESS', `${API_ADDRESS_URL}/${address}`);
		return get(`${API_ADDRESS_URL}/${address}`, { responseType: 'json' })
			.then(function (response) {
				// handle success
				// console.log('api.getAddresses', response.data);
				return response.data;
			})
			.catch(function (error) {
				// handle error
				console.log(error);
				throw error;
			});
	};
	getTransactions = () => {
		return get(API_TRANSACTIONS_URL, { responseType: 'json' })
			.then(function (response) {
				// handle success
				// console.log('api.getTransactions', response.data);
				return response.data;
			})
			.catch(function (error) {
				// handle error
				console.log(error);
				throw error;
			});
	};
	sendTransaction = (fromAddress, toAddress, amount) => {
		return post(
			API_TRANSACTIONS_URL,
			{
				fromAddress: fromAddress,
				toAddress: toAddress,
				amount: amount.toString(),
			},
			{ responseType: 'json' }
		)
			.then(function (response) {
				// handle success
				// console.log('api.sendTransaction', response.data);
				if (response.status !== 200) throw new Error(response.data.statusText);

				return response.data;
			})
			.catch(function (error) {
				// handle error
				console.log(error);
				throw error;
			});
	};

	cleanupTests = (deposit, house, fees, sender, addresses) => {
		return this.getAddresses(deposit)
			.then((d) => {
				console.log('deposit', d);
				if (d.balance && parseFloat(d.balance) > 0)
					return this.sendTransaction(deposit, sender, d.balance.toString());
			})
			.then(() => {
				return this.getAddresses(house);
			})
			.then((h) => {
				console.log('house', h);
				if (h.balance && parseFloat(h.balance) > 0)
					return this.sendTransaction(house, sender, h.balance.toString());
			})
			.then(() => {
				return this.getAddresses(fees);
			})
			.then((f) => {
				console.log('fees', f);
				if (f.balance && parseFloat(f.balance) > 0)
					return this.sendTransaction(fees, sender, f.balance.toString());
			})
			.then(() => {
				return addresses.forEach((a) => {
					return this.getAddresses(a).then((r) => {
						console.log('recipient', r);
						if (r.balance && parseFloat(r.balance) > 0)
							return this.sendTransaction(a, sender, r.balance.toString());
					});
				});
			})
			.then(() => {
				return { status: 'COMPLETED' };
			})
			.catch((error) => {
				console.log(error);
				throw error;
			});
	};
}
export default API;
