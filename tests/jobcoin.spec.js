#!/usr/bin/env node
// 'use strict';
import { expect } from 'chai';
import utils from '../utils.js';
import MixerJob from '../mixer.js';
import API from '../apiClient.js';
// import MockAPI from '../mockApiClient.js';
// import TestData from '../testData.js';

const HOUSE_ADDRESS = 'house';
const FEE_ADDRESS = 'fees';
const SENDER = 'Gilligan';
const MIN_INTERVAL = 100;
const MAX_INTERVAL = 2000;
const depositAddress = '50920c25'; //utils.generateDepositAddress();
const addresses = ['Skipper', 'Ginger', 'Maryanne'];
// const testData = TestData(depositAddress);
// let api = new MockAPI(testData);
const api = new API();

describe('utils', () => {
	it('should exist', () => {
		expect(utils).to.be.ok;
	});
	it('generateDepositAddress generates a string with 8 characters', () => {
		const depAddress = utils.generateDepositAddress();
		expect(typeof depositAddress).to.equal('string');
		expect(depAddress).to.have.length(8);
	});
});

describe('test mixer ', () => {
	//Clean up the data
	// api
	// 	.cleanupTests(
	// 		depositAddress,
	// 		HOUSE_ADDRESS,
	// 		FEE_ADDRESS,
	// 		SENDER,
	// 		testData.sendAddresses
	// 	)
	// 	.then((status) => {
	// 		expect(status.status).to.equal('COMPLETED');
	// 	});

	//Time to test!
	let count = parseInt(addresses.length, 10);
	let mixer = new MixerJob(api, addresses, depositAddress);
	let amount = 37.5;
	let mixedAmounts = mixer.mixBalance(amount, count);
	it(`should have ${count} amounts`, () => {
		// console.log(mixedAmounts);
		expect(mixedAmounts.length).to.equal(count);
	});
	it('should mix balances', () => {
		let sum = mixedAmounts.reduce((acc, val) => acc + val, 0);
		expect(sum).to.equal(amount);
	});
	it('should create random intervals ', () => {
		let intervals = mixer.randomIntervals(MIN_INTERVAL, MAX_INTERVAL, count);
		expect(intervals.filter((i) => i < MIN_INTERVAL).length).to.equal(0);
		expect(intervals.filter((i) => i > MAX_INTERVAL).length).to.equal(0);
		expect(intervals.length).to.equal(count);
	});
	it('should mix and send ', () => {
		mixer.poll().then((result) => {
			expect(result.status).to.equal('complete');
		});
	});
	it('should mix and send ', () => {
		expect(result.status).to.equal('complete');
	});
});
