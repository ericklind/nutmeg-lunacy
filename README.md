# Nutmeg Lunacy

## Welcome to Nutmeg Lunacy - A Jobcoin Mixer!

### Runing the mixer:

- Run "npm start" or "yarn start" to run the mixer app.

You will be asked for:

- A comma sperated list of address strings.

A deposit address will returned. Any amount sent to the deposit address will mixed, split, and send to the list of addresses.

### Running the client

You can send coins from the client cli or use the [website](https://jobcoin.gemini.com/nutmeg-lunacy)

- Run "npm client" or "yarn client" to run the send client.

You will be asked for:

- The address you are sending from
- The address you are sending to
- The amount you wish to send

### Running tests

- Run "npm test" or "yarn test" to run the tests.

## Notes

Next steps:

- Tighten up the validation
- Improve mixing algorithm
- Figure out async testing in mocha
