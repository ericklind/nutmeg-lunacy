import chalk from 'chalk';
import pkg from 'inquirer';
import utils from './utils.js';
import Runner from './runner.js';
import MixerJob from './mixer.js';
import API from './apiClient.js';

const { prompt: _prompt } = pkg;
const { green } = chalk;
const POLL_INTERVAL = 10000;
const runner = new Runner();

function prompt() {
	/* Inquirer documentation: https://github.com/SBoudrias/Inquirer.js#documentation */
	let depositAddress = utils.generateDepositAddress();
	_prompt([
		{
			name: 'addresses',
			message:
				'Please enter a comma-separated list of new, unused Jobcoin addresses where your mixed Jobcoins will be sent:',
		},
		{
			name: 'deposit',
			message: `You may now send Jobcoins to address ${green(depositAddress)}.
They will be mixed and sent to your destination addresses. 
Enter ${green('"y"')} to save and to add another job.`,
			when: (answers) => answers.addresses,
		},
	]).then((answers) => {
		let a = answers.addresses.toString().split(',');
		let api = new API();
		let job = new MixerJob(api, a, depositAddress);
		runner.add(job);

		let { deposit } = answers;
		if (deposit && deposit.toLowerCase() === 'y') {
			prompt();
		} else {
			runner.poll(); //Force the poll and exit
		}
	});
}

console.log('Welcome to the Jobcoin mixer!');
runner.start(POLL_INTERVAL);

prompt();

export default prompt;
