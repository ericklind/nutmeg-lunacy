class Runner {
	constructor() {
		this.jobs = [];
		this.intervalId = null;
	}
	add = (job) => {
		this.jobs.push(job);
	};
	poll = () => {
		this.jobs.forEach((j) => {
			j.poll();
		});
	};
	start = (interval) => {
		if (this.intervalId) this.stop();
		this.intervalId = setInterval(this.poll, interval);
	};
	stop = () => {
		clearInterval(this.intervalId);
	};
}
export default Runner;
